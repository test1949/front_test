package com.example.beerservice.model;

import java.util.List;

public class Ingredients {
/*
 * /*
      * "ingredients":{"malt":[{"name":"Pilsner","amount":{"value":1.69,"unit":"kilograms"}},{"name":"Wheat","amount":{"value":0.38,"unit":"kilograms"}},
      * {"name":"Flaked Oats","amount":{"value":0.13,"unit":"kilograms"}}],
      * "hops":[{"name":"Chinook","amount":{"value":2.5,"unit":"grams"},
      * "add":"start","attribute":"bitter"},
      * {"name":"Amarillo","amount":{"value":37.5,"unit":"grams"},"add":"end","attribute":"flavour"},{"name":"Chinook","amount":{"value":25,"unit":"grams"},"add":"end","attribute":"flavour"},{"name":"Mosaic","amount":{"value":37.5,"unit":"grams"},"add":"end","attribute":"flavour"},
      * {"name":"Amarillo","amount":{"value":62.5,"unit":"grams"},"add":"dry hop","attribute":"aroma"},
      * {"name":"Chinook","amount":{"value":62.5,"unit":"grams"},"add":"dry hop","attribute":"aroma"},
      * {"name":"Mosaic","amount":{"value":62.5,"unit":"grams"},"add":"dry hop","attribute":"aroma"}]
      * ,"yeast":"Wyeast 1056 - American Ale™"}
 */
	private List<Malt> malt ;
	private List<Hops> hops;
	private String yeast;
	
	public List<Malt> getMalt() {
		return malt;
	}
	public void setMalt(List<Malt> malt) {
		this.malt = malt;
	}
	public List<Hops> getHops() {
		return hops;
	}
	public void setHops(List<Hops> hops) {
		this.hops = hops;
	}
	public String getYeast() {
		return yeast;
	}
	public void setYeast(String yeast) {
		this.yeast = yeast;
	}
	
	
}
