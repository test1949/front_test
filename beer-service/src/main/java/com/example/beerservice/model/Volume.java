package com.example.beerservice.model;

public class Volume {
/*
 * "volume": {
      "value": 20,
      "unit": "liters"
    }
 */
	
	private Integer value ;
	private String unit;
	
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	
}
