package com.example.beerservice.model;

public class Malt {
/*
 * {"malt":[{"name":"Pilsner","amount":{"value":1.69,"unit":"kilograms"}}
 */
	private String name ;
	private Amount amount;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Amount getAmount() {
		return amount;
	}
	public void setAmount(Amount amount) {
		this.amount = amount;
	}
	
	
}
