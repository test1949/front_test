package com.example.beerservice.model;

import java.util.List;

public class Method {

	/*
	 * "method":
	 * {"mash_temp":[
	 * {"temp":{"value":65,"unit":"celsius"},
	 * "duration":75}],
	 * "fermentation":{"temp":{"value":21,"unit":"celsius"}},
	 * "twist":null}
	 
	 */
	
	  private List<Mash_temp> mash_temp;
	  private Fermentation fermentation;
	  private Object twist ;
	  
	  
	public List<Mash_temp> getMash_temp() {
		return mash_temp;
	}
	public void setMash_temp(List<Mash_temp> mash_temp) {
		this.mash_temp = mash_temp;
	}
	public Fermentation getFermentation() {
		return fermentation;
	}
	public void setFermentation(Fermentation fermentation) {
		this.fermentation = fermentation;
	}
	public Object getTwist() {
		return twist;
	}
	public void setTwist(Object twist) {
		this.twist = twist;
	}
	  
	  
}
