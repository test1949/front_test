package com.example.beerservice.model;

public class Fermentation {
/*
 * "fermentation":{"temp":{"value":21,"unit":"celsius"}}
 */
	private Temp temp;

	public Temp getTemp() {
		return temp;
	}

	public void setTemp(Temp temp) {
		this.temp = temp;
	}
	
	
}
