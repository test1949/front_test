package com.example.beerservice.model;

public class Hops {
/*
 * "hops":[{"name":"Chinook","amount":{"value":2.5,"unit":"grams"},
      * "add":"start","attribute":"bitter"},
      * {"name":"Amarillo","amount":{"value":37.5,"unit":"grams"},"add":"end","attribute":"flavour"},{"name":"Chinook","amount":{"value":25,"unit":"grams"},"add":"end","attribute":"flavour"},{"name":"Mosaic","amount":{"value":37.5,"unit":"grams"},"add":"end","attribute":"flavour"},
      * {"name":"Amarillo","amount":{"value":62.5,"unit":"grams"},"add":"dry hop","attribute":"aroma"},
      * {"name":"Chinook","amount":{"value":62.5,"unit":"grams"},"add":"dry hop","attribute":"aroma"},
      * {"name":"Mosaic","amount":{"value":62.5,"unit":"grams"},"add":"dry hop","attribute":"aroma"}]
 */
	private String name ;
	private Amount amount;
	private String add ;
	private String attribute;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Amount getAmount() {
		return amount;
	}
	public void setAmount(Amount amount) {
		this.amount = amount;
	}
	public String getAdd() {
		return add;
	}
	public void setAdd(String add) {
		this.add = add;
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	
	
}
