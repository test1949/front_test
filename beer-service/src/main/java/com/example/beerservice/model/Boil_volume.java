package com.example.beerservice.model;

public class Boil_volume {

/*
 * "boil_volume": {
      "value": 25,
      "unit": "liters"
    },
 */
	
	private Integer value ;
	private String unit ;
	
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	
}
