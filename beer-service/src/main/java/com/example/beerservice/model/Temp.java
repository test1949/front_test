package com.example.beerservice.model;

public class Temp {

/*
 * {"temp":{"value":21,"unit":"celsius"}}
 */
	private Integer value ;
	private String unit ;
	
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	
}
