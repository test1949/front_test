package com.example.beerservice.model;

public class Amount {
/*
 * "amount":{"value":1.69,"unit":"kilograms"}
 */
   private Double value ;
   private String kilograms;
   
public Double getValue() {
	return value;
}
public void setValue(Double value) {
	this.value = value;
}
public String getKilograms() {
	return kilograms;
}
public void setKilograms(String kilograms) {
	this.kilograms = kilograms;
}
   
   
}
