package com.example.beerservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.beerservice.model.Beer;

@RestController
public class Mycontroller {

	@Autowired
	private RestTemplate restTemplate;

	private final String USER_API = "https://api.punkapi.com/v2/beers";

	@GetMapping("/beers/{id}")
	public Beer[] getBeerById(@PathVariable Integer id) {
		String url = USER_API + "/{id}";
		return restTemplate.getForObject(url, Beer[].class, id);
	}
    
	@GetMapping("/beer/{name}")
	public Beer[] getBeerByName(@PathVariable String name) {
		String url = USER_API +"?beer_name="+name;
		return restTemplate.getForObject(url, Beer[].class );
	}
	
	@GetMapping("/beers/")
	public Beer[] getAllBeers() {
		return restTemplate.getForObject(USER_API, Beer[].class);

	}
}


