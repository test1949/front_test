# Webapp partie Backend : Spring boot


# Goal:

User enters a name then he can see the list of beers containing this name. 

Beer list should at least include a name, description and an image.

When user clicks on an item of the list, he can see the detail of it (alcohol percentage,
ingredient,...)

Beer api : ​ https://punkapi.com/documentation/v2



# Prerequisites:

Java 11

Spring Tools Suite (STS)

# Steps:

1. Download project 

2. Inside Eclipse or STS

      File -> Import -> Maven -> Existing Maven project

      Run the application main method by right clicking on it and choosing Run As -> Spring Boot App.
